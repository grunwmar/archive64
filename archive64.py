import re, os, base64, sys
from dataclasses import dataclass
import xml.dom.minidom as xdm
import hashlib

def log(string=""):
    print(">",string, '\u001b[0m')

@dataclass
class DataBearer:
    data: str
    mime: str
    hash: str

def parse_list(filename):
    log('\u001b[1m\u001b[38;5;34mOpening\u001b[0m \u001b[1m{}'.format(filename))
    if os.path.isfile(filename):
        r_parse_title = re.compile("\s*TITLE=\s*(.*)")
        r_parse_descr = re.compile("\s*DESCRIPTION=\s*(.*)")
        r_parse_files = re.compile("\s*(.*),\s*(.*),\s*(.*)")
        r_dict = {}
        with open(filename, "r") as file:
            r = file.read()
            title = r_parse_title.findall(r)
            descr = r_parse_descr.findall(r)
            files = r_parse_files.findall(r)
            r_dict.update({
                    "files": files,
                    "title": title[0] if len(title) > 0 else None,
                    "descr": descr[0] if len(descr) > 0 else None})
        log('\u001b[1m\u001b[38;5;83mOpen')
        return r_dict
    else:
        log('File "{}" not found.'.format(filename))
        return None

def load_file_to_bytes(filename):
    bytes = bytearray()
    if os.path.isfile(filename):
        with open(filename, "rb") as f:
            log('\u001b[1m\u001b[38;5;92mReading\u001b[0m \u001b[1m{}'.format(filename))
            bytes.extend(f.read())
    else:
        log('File "{}" not fount'.format(filename))
    return bytes


def get_data64(filename):

    if os.path.isfile(filename):
        log()
        mime = None
        bytes = load_file_to_bytes(filename)
        hash_func = hashlib.sha256()
        hash_func.update(bytes)
        log('\u001b[1m\u001b[38;5;126mConverting\u001b[0m \u001b[1m')
        encoded = str(base64.b64encode(bytes))[2:-1]

        data_bearer = None

        if bytes is not None:
            data_bearer = DataBearer(mime=mime, data=encoded, hash=hash_func.hexdigest())
        return data_bearer
    else:
        return None


def load_package(filename):
    
    css_code = ""
    js_code = ""

    realPath = os.path.realpath(__file__) 
    dirPath = os.path.dirname(realPath)

    with open(os.path.join(dirPath, 'css_source.css'), 'r') as css_file:
        for css_line in css_file.read():
            css_code += str(css_line)
    
    with open(os.path.join(dirPath, 'handler.js'), 'r') as js_file:
        for js_line in js_file.read():
            js_code += str(js_line)

    trim_white = re.compile('\s+', 0)

    css_code = trim_white.sub(" ", css_code)
    js_code  = trim_white.sub(" ", js_code)
           

    if os.path.isfile(filename):
        def_package = parse_list(filename)
        file_list = []

        package_title = def_package["title"]
        package_descr = def_package["descr"]


        for file in def_package["files"]:
            name = file[0]
            file_description = file[2]
            mime = file[1]
        
            data = get_data64(name)

            if data is not None:
                file_list.append((file_description, mime, "data:{};base64,{}".format(mime, data.data), data.hash))

        objects = ""
        data_storage = ""

        length = len(file_list)
        for n, obj in enumerate(file_list):
            i = str(n+1)
            newline = "\n" if n+1 < length else ""
            objects += """              <tr ondblclick="xshow('{name}')">
                <td>{descr}</td>
                <td>{mime}</td>
                <td>
                    <input value="{hash_str}" onclick="this.select()"/>
                </td>
                <td>
                    <a onclick="xshow('{name}')">show</a>
                    <a onclick="xdownload('{name}')">save</a>
                </td>
              </tr>{newline}""".format(index=i, descr=obj[0], mime=obj[1], name="file_"+i, hash_str=obj[3], newline=newline)
            data_storage += """          <div id="{name}" data-description="{descr}" data-mime-type="{mime}">\n             {data_url}\n          </div>{newline}""".format(name="file_"+i, mime=obj[1], descr=obj[0], data_url=obj[2], newline=newline)


        html_template = """<!doctype html>
<html>
    <head>
        <title>@{title}</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <style>
            {css}
        </style>
        <script>
            {js}
        </script>
    </head>
    <body>
        <header>
            <h1>HTML package</h1>
            <h3>{title}</h3>{description}
        </header>
        <main>
            <table>
              <tr>
                <th>[description]</th>
                <th>[mime type]</th>
                <th>[SHA-256]</th>
                <th></th>
              </tr>
{objects}
            </table>
            <div id="showroom"></div>
        </main>
        <div class="data-field">
{data_storage}
        </div>
    </body>
</html>
    """

        src = html_template.format(data_storage=data_storage, objects=objects, title=package_title, description='\n            <div class="descr">{}</div>'.format(package_descr), css=css_code, js=js_code)
        log()
        log('\u001b[1m\u001b[38;5;32mWriting package\u001b[0m \u001b[1m{}.pkg.html'.format(filename))
        with open('./{}.pkg.html'.format(filename), 'w+') as write_file:
            write_file.write(src)


if len(sys.argv) >= 2:
    load_package(sys.argv[1])
    log('\u001b[1m\u001b[38;5;81mDone\u001b[0m')
