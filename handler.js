function closex() {
    document.getElementById('showroom').innerHTML = "";
}

function xshow(name) {
    let file = document.getElementById(name);
    let closeButton = document.createElement('input');

    closeButton.setAttribute('type', 'button');
    closeButton.setAttribute('value', 'close');
    closeButton.setAttribute('class', 'close');
    closeButton.addEventListener("click", function () { closex() });

    let mediaObject = document.createElement('object');
    mediaObject.setAttribute('type', file.getAttribute('data-mime-type'));
    mediaObject.setAttribute('data', file.innerText);
    mediaObject.setAttribute('id', 'screen');

    document.getElementById('showroom').appendChild(closeButton);
    document.getElementById('showroom').appendChild(mediaObject);
}

function xdownload(name) {
    let file = document.getElementById(name);
    let link = document.createElement("a");

    link.download = file.getAttribute('data-description');
    link.href = file.innerText;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    
    delete link;

}
